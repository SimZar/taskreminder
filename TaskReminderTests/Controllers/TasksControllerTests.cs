using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskReminder.Controllers;
using TaskReminder.Data;
using TaskReminder.DataContracts;

namespace TaskReminderTests.Controllers
{
    [TestClass]
    public class TasksControllerTests
    {
        private TasksController _controller;
        private DbContextOptionsBuilder _builder;
        private ApplicationDbContext _dbContext;

        [TestInitialize]
        public void TestInitialize()
        {
            _builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            _builder.UseSqlServer("Server=920e5d40-17ca-41cd-88ae-a83a016dcbf4.sqlserver.sequelizer.com;Database=db920e5d4017ca41cd88aea83a016dcbf4;User ID=phsjcvzwbavglnwo;Password=YwE4fQBYRrVDHsEBbmUEc7tdVXpSu4VV6qUWQgp3X2MVdytRDxpoUvFZcBgpARDW;");
            _dbContext = new ApplicationDbContext(_builder.Options);
            _controller = new TasksController(new TasksRepository(_dbContext));
        }

        [TestMethod]
        public void CreateTask()
        {
            // Arrange
            var selectedSubject = _dbContext.Subjects.FirstOrDefault();
            var taskRequest = new TaskRequestResponse
            {
                Complexity = 1,
                Description = "description",
                DueDate = DateTime.Now
            };

            // Act
            _controller.Post(taskRequest, selectedSubject.Id);

            // Assert
            var selectedSameSubject = _dbContext
                .Subjects
                .Include(s => s.Tasks)
                .Single(s => s.Id == selectedSubject.Id);
            var tasksList = selectedSameSubject.Tasks;
            var expectedTask = tasksList.Find(t =>
                t.Complexity == taskRequest.Complexity && 
                t.Description == taskRequest.Description &&
                t.DueDate == taskRequest.DueDate);

            Assert.IsNotNull(expectedTask);
        }

        [TestMethod]
        public void EditTask()
        {
            // Arrange
            var selectedTask = _dbContext.Tasks.FirstOrDefault();
            var id = selectedTask.Id;
            var complexity = selectedTask.Complexity;
            var description = selectedTask.Description;
            var dueDate = selectedTask.DueDate;

            var updateRequest = new TaskRequestResponse
            {
                Complexity = complexity + 1,
                Description = description + "updated",
                DueDate = dueDate.AddMinutes(1)
            };

            // Act
            _controller.Update(id, updateRequest);

            // Assert
            var updatedTask = _dbContext.Tasks.Find(id);

            Assert.AreEqual(complexity + 1, updatedTask.Complexity);
            Assert.AreEqual(description + "updated", updatedTask.Description);
            Assert.AreEqual(dueDate.AddMinutes(1), updatedTask.DueDate);
        }

        [TestMethod]
        public void DeleteTask()
        {
            // Arrange
            var taskToDelete = _dbContext.Tasks.FirstOrDefault();
            var id = taskToDelete.Id;

            // Act
            _controller.Delete(id);

            // Assert
            var checkForTask = _dbContext.Tasks.Find(id);

            Assert.IsNull(checkForTask);
        }

        [TestMethod]
        public void GetTasks()
        {
            // Arrange
            if (_dbContext.Tasks.ToList().Count < 2)
            {
                CreateTask();
                CreateTask();
            }

            // Act
            var tasksList = _controller.GetAll();

            // Assert
            Assert.IsNotNull(tasksList);
            Assert.IsTrue(tasksList.Count >= 2);
        }
    }
}
