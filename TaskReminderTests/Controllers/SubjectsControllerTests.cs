using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskReminder.Controllers;
using TaskReminder.Data;
using TaskReminder.Data.Entities;
using TaskReminder.DataContracts;

namespace TaskReminderTests.Controllers
{
    [TestClass]
    public class SubjectsControllerTests
    {
        private SubjectsController _controller;
        private DbContextOptionsBuilder _builder;
        private ApplicationDbContext _dbContext;

        [TestInitialize]
        public void TestInitialize()
        {
            _builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            _builder.UseSqlServer("Server=920e5d40-17ca-41cd-88ae-a83a016dcbf4.sqlserver.sequelizer.com;Database=db920e5d4017ca41cd88aea83a016dcbf4;User ID=phsjcvzwbavglnwo;Password=YwE4fQBYRrVDHsEBbmUEc7tdVXpSu4VV6qUWQgp3X2MVdytRDxpoUvFZcBgpARDW;");
            _dbContext = new ApplicationDbContext(_builder.Options);
            _controller = new SubjectsController(new SubjectsRepository(_dbContext));
        }

        [TestMethod]
        public void CreateNewSubject()
        {
            // Arrange
            var selectedUser = _dbContext.Users.FirstOrDefault();
            string username = selectedUser.UserName;

            var subjectToPost = new SubjectRequestResponse
            {
                Name = "New Subject",
                Code = "123"
            };

            // Act
            _controller.Post(subjectToPost, username);
            
            // Assert
            var subjects = _dbContext.Subjects.ToList();
            var expectedSubject = subjects.Find(s => s.Name.Equals(subjectToPost.Name) &&
                                                     s.Code.Equals(subjectToPost.Code));

            Assert.IsNotNull(expectedSubject);
        }

        [TestMethod]
        public void EditSubject()
        {
            // Arrange
            var selectedSubject = _dbContext.Subjects.FirstOrDefault();
            string oldName = selectedSubject.Name;
            string oldCode = selectedSubject.Code;

            var updateRequest = new SubjectRequestResponse
            {
                Name = oldName + "updated",
                Code = oldCode + "updated"
            };

            // Act
            _controller.Update(selectedSubject.Id, updateRequest);

            // Assert
            var updatedSubject = _dbContext.Subjects.Single(s => s.Id == selectedSubject.Id);

            Assert.AreEqual(oldName + "updated", updatedSubject.Name);
            Assert.AreEqual(oldCode + "updated", updatedSubject.Code);
        }

        [TestMethod]
        public void DeleteSubject()
        {
            // Arange
            var subjectToDelete = _dbContext.Subjects.FirstOrDefault();
            var id = subjectToDelete.Id;

            // Act
            _controller.Delete(id);

            // Assert
            var checkForSubject = _dbContext.Subjects.Find(id);

            Assert.IsNull(checkForSubject);
        }

        [TestMethod]
        public void GetAllSubjects()
        {
            // Arrange
            if (_dbContext.Subjects.ToList().Count < 2)
            {
                CreateNewSubject();
                CreateNewSubject();
            }

            // Act
            var subjectsList = _controller.GetAll();

            // Assert
            Assert.IsNotNull(subjectsList);
            Assert.IsTrue(subjectsList.Count >= 2);
        }

    }
}
