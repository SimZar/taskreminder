﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using TaskReminder.Data;
using TaskReminder.DataContracts;

namespace TaskReminder.Controllers
{
    [Authorize(ActiveAuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/tasks")]
    public class TasksController : Controller
    {
        private readonly TasksRepository _tasksRepository;

        public TasksController(TasksRepository tasksRepository)
        {
            _tasksRepository = tasksRepository;
        }

        [HttpGet]
        public List<TaskRequestResponse> GetAll()
        {
            return _tasksRepository.GetAll();
        }

        [HttpGet("subject/{id}")]
        public IActionResult GetBySubject(int id)
        {
            try
            {
                var subjects = _tasksRepository.GetBySubject(id);
                return Ok(subjects);
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            try
            {
                var task = _tasksRepository.GetById(id);
                return Ok(task);
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
        }

        [HttpPost("{subjectId}")]
        public IActionResult Post([FromBody]TaskRequestResponse request, int subjectId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                _tasksRepository.Post(request, subjectId);
                return NoContent();
            }
            catch (InvalidOperationException e)
            {
                return StatusCode((int)HttpStatusCode.Conflict, new { ErrorMessage = e.Message });
            }
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody]TaskRequestResponse request)
        {
           if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                _tasksRepository.Update(id, request);
                return NoContent();
            }
            catch (InvalidOperationException e)
            {
                return StatusCode((int)HttpStatusCode.Conflict, new { ErrorMessage = e.Message });
            }
    }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _tasksRepository.Delete(id);
                return NoContent();
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
        }

    }
}
