﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using IdentityModel;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using TaskReminder.Data;
using TaskReminder.Data.Entities;
using TaskReminder.DataContracts;

namespace TaskReminder.Controllers
{
    [Authorize(ActiveAuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/subjects")]
    public class SubjectsController : Controller
    {
        private readonly SubjectsRepository _subjectsRepository;

        public SubjectsController(SubjectsRepository subjectsRepository)
        {
            _subjectsRepository = subjectsRepository;
        }

        [HttpGet]
        public List<SubjectRequestResponse> GetAll()
        {
            return _subjectsRepository.GetAll();
        }

        [HttpGet("user/{username}")]
        public IActionResult GetByUser(string username)
        {
            try
            {
                var subjects = _subjectsRepository.GetByUser(username);
                return Ok(subjects);
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            try
            {
                var subject = _subjectsRepository.GetById(id);
                return Ok(subject);
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
        }

        [HttpPost("{username}")]
        public IActionResult Post([FromBody]SubjectRequestResponse request, string username)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                _subjectsRepository.Post(request, username);
                return NoContent();
            }
            catch (InvalidOperationException e)
            {
                return StatusCode((int)HttpStatusCode.Conflict, new { ErrorMessage = e.Message });
            }
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody]SubjectRequestResponse request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                _subjectsRepository.Update(id, request);
                return NoContent();
            }
            catch (InvalidOperationException e)
            {
                return StatusCode((int)HttpStatusCode.Conflict, new { ErrorMessage = e.Message });
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _subjectsRepository.Delete(id);
                return NoContent();
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
        }
    }
}
