﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TaskReminder.DataContracts
{
    public class TaskRequestResponse
    {
        public int Id { get; set; }
        [Required]
        public int Complexity { get; set; }
        [Required]
        public DateTime DueDate { get; set; }
        public string Description { get; set; }

        public bool IsDone { get; set; }
    }
}
