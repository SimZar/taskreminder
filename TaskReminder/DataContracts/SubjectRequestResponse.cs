﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TaskReminder.DataContracts
{
    public class SubjectRequestResponse
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Code { get; set; }
        public List<TaskRequestResponse> Tasks { get; set; }
    }
}
