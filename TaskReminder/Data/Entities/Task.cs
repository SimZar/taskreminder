﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TaskReminder.Data.Entities
{
    public class Task
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int Complexity { get; set; }
        [Required]
        public DateTime DueDate { get; set; }
        public string Description { get; set; }

        public bool IsDone { get; set; }

        public int SubjectId { get; set; }
        public Subject Subject { get; set; }
    }
}
