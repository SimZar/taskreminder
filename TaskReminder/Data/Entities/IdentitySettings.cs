﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskReminder.Data.Entities
{
  public class IdentitySettings
  {
    public string SiteUrl { get; set; }
    public string Key { get; set; }
  }
}
