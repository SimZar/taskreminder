﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TaskReminder.Data.Entities;
using TaskReminder.DataContracts;

namespace TaskReminder.Data
{
    public class TasksRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public TasksRepository(ApplicationDbContext appDbContext)
        {
            _dbContext = appDbContext;
        }
        public List<TaskRequestResponse> GetAll()
        {

            return _dbContext.Tasks.Select(task => new TaskRequestResponse
            {
                Id = task.Id,
                Complexity = task.Complexity,
                Description = task.Description,
                DueDate = task.DueDate,
                IsDone = task.IsDone
            })
                .ToList();
        }

        public List<TaskRequestResponse> GetBySubject(int subjectId)
        {
            return _dbContext.Subjects
                .Include(subject => subject.Tasks)
                .Single(subject => subject.Id == subjectId)
                .Tasks
                .Select(task => new TaskRequestResponse
                {
                    Id = task.Id,
                    Complexity = task.Complexity,
                    Description = task.Description,
                    DueDate = task.DueDate,
                    IsDone = task.IsDone
                })
                .ToList();
        }

        public TaskRequestResponse GetById(int id)
        {
            var task = _dbContext.Tasks.Single(t => t.Id == id);

            return new TaskRequestResponse
            {
                Id = task.Id,
                Complexity = task.Complexity,
                Description = task.Description,
                DueDate = task.DueDate,
                IsDone = task.IsDone
            };
        }

        public void Post(TaskRequestResponse request, int subjectId)
        {
            var subject = _dbContext.Subjects
                .Include(s => s.Tasks)
                .Single(s => s.Id == subjectId);

            subject.Tasks.Add(new Task
            {
                Complexity = request.Complexity,
                Description = request.Description,
                DueDate = request.DueDate,
                Subject = subject,
                SubjectId = subject.Id,
                IsDone = request.IsDone
            });

            _dbContext.SaveChanges();
        }

        public void Update(int id, TaskRequestResponse request)
        {
            var task = _dbContext.Tasks.Single(t => t.Id == id);

            task.Description = request.Description;
            task.DueDate = request.DueDate;
            task.Complexity = request.Complexity;
            task.IsDone = request.IsDone;

            _dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            var task = _dbContext.Tasks.Single(t => t.Id == id);

            _dbContext.Tasks.Remove(task);

            _dbContext.SaveChanges();
        }
    }
}
