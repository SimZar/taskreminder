﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using TaskReminder.Data.Entities;
using TaskReminder.DataContracts;

namespace TaskReminder.Data
{
    public class SubjectsRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public SubjectsRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<SubjectRequestResponse> GetAll()
        {
            var subjects = _dbContext.Subjects
                .Include(s => s.Tasks)
                .ToList();

            var result = new List<SubjectRequestResponse>();

            foreach (var subject in subjects)
            {
                var s = new SubjectRequestResponse
                {
                    Id = subject.Id,
                    Code = subject.Code,
                    Name = subject.Name,
                    Tasks = new List<TaskRequestResponse>()
                };

                foreach (var task in subject.Tasks)
                {
                    s.Tasks.Add(new TaskRequestResponse
                    {
                        Id = task.Id,
                        Complexity = task.Complexity,
                        Description = task.Description,
                        DueDate = task.DueDate
                    });
                }
                result.Add(s);
            }

            return result;
        }

        public SubjectRequestResponse GetById(int id)
        {
            var subject = _dbContext.Subjects
                .Include(s => s.Tasks)
                .Single(s => s.Id == id);

            var result = new SubjectRequestResponse
            {
                Id = subject.Id,
                Code = subject.Code,
                Name = subject.Name,
                Tasks = new List<TaskRequestResponse>()
            };

            foreach (var task in subject.Tasks)
            {
                result.Tasks.Add(new TaskRequestResponse
                {
                    Id = task.Id,
                    Complexity = task.Complexity,
                    Description = task.Description,
                    DueDate = task.DueDate
                });
            }

            return result;
        }

        public List<SubjectRequestResponse> GetByUser(string username)
        {
            var user = _dbContext.ApplicationUsers
                .Include(u => u.Subjects)
                .ThenInclude(s => s.Tasks)
                .Single(u => u.UserName == username);

            var result = new List<SubjectRequestResponse>();

            foreach (var subject in user.Subjects)
            {
                var r = new SubjectRequestResponse
                {
                    Id = subject.Id,
                    Code = subject.Code,
                    Name = subject.Name,
                    Tasks = new List<TaskRequestResponse>()
                };

                foreach (var task in subject.Tasks)
                {
                    r.Tasks.Add(new TaskRequestResponse
                    {
                        Id = task.Id,
                        Complexity = task.Complexity,
                        Description = task.Description,
                        DueDate = task.DueDate
                    });
                }

                result.Add(r);
            }

            return result;
        }

        public void Post(SubjectRequestResponse request, string username)
        {
            var user = _dbContext.ApplicationUsers
                .Include(u => u.Subjects)
                .Single(u => u.UserName == username);

            user.Subjects.Add(new Subject
            {
                ApplicationUser = user,
                ApplicationUserId = user.Id,
                Code = request.Code,
                Name = request.Name,
                Tasks = new List<Task>()
            });

            _dbContext.SaveChanges();
        }

        public void Update(int id, SubjectRequestResponse request)
        {
            var subject = _dbContext.Subjects
                .Include(s => s.Tasks)
                .Single(s => s.Id == id);

            subject.Code = request.Code;
            subject.Name = request.Name;

            _dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            var subject = _dbContext.Subjects
                .Include(s => s.Tasks)
                .Single(s => s.Id == id);

            _dbContext.Subjects.Remove(subject);
            _dbContext.SaveChanges();
        }
    }
}
