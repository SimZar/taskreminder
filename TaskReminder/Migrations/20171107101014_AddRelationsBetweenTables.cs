﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TaskReminder.Migrations
{
    public partial class AddRelationsBetweenTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ApplicationUserId",
                table: "Subjects",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserId1",
                table: "Subjects",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Subjects_ApplicationUserId1",
                table: "Subjects",
                column: "ApplicationUserId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Subjects_AspNetUsers_ApplicationUserId1",
                table: "Subjects",
                column: "ApplicationUserId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subjects_AspNetUsers_ApplicationUserId1",
                table: "Subjects");

            migrationBuilder.DropIndex(
                name: "IX_Subjects_ApplicationUserId1",
                table: "Subjects");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId",
                table: "Subjects");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId1",
                table: "Subjects");
        }
    }
}
